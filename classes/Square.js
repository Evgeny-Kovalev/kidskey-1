import Figure from './Figure.js'

export default class Square extends Figure {
    constructor(x, y) {
        super(x, y);
    }

    printMain() {
        this.lineStyle(1, 0xFEEB77, 1);
        this.beginFill(0xffffff);
        this.drawRect(0, 0, 60, 60);
        this.endFill();
    }

    print() {
        super.print();
        this.lineStyle(1, 0xFEEB77, 1);
        this.beginFill(Math.random() * 0xffffff, 1, 1);
        this.drawRect(0, 0, 40, 40);
        this.endFill();
    }
}