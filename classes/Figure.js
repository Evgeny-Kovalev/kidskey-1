export default class Figure extends PIXI.Graphics {
    constructor(x, y) {
        super();
        this.x = x;
        this.y = y;
        this.posX = x;
        this.posY = y;
    }

    print() {
        this.scale.set(Math.random() * 1.5 + 0.5);
        this.rotation = Math.random() * 10;
    }
}
