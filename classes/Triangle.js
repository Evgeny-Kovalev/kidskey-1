import Figure from './Figure.js'

export default class Triangle extends Figure {
    constructor(x, y) {
        super(x, y);
    }

    printMain() {
        this.lineStyle(1, 0xFEEB77, 1);
        this.beginFill(0xffffff);
        this.moveTo(0, 0);
        this.lineTo(50, 50);
        this.lineTo(0, 50);
        this.lineTo(0, 0);
        this.endFill();
    }

    print() {
        super.print();
        this.lineStyle(1, 0xFEEB77, 1);
        this.beginFill(Math.random() * 0xffffff, 1, 1);
        this.moveTo(0, 0);
        this.lineTo(50, 50);
        this.lineTo(0, 50);
        this.lineTo(0, 0);
        this.endFill();
    }
}