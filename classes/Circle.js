import Figure from './Figure.js'

export default class Circle extends Figure {
    constructor(x, y) {
        super(x, y);
    }

    printMain() {
        // Circle + line style 1
        this.lineStyle(1, 0xFEEB77, 1);
        this.beginFill(0xffffff);
        this.drawCircle(0, 0, 30);
        this.endFill();
    }

    print() {
        super.print();
        this.lineStyle(1, 0xFEEB77, 1);
        this.beginFill(Math.random() * 0xffffff, 1, 1);
        this.drawCircle(0, 0, 20);
        this.endFill();
    }
}