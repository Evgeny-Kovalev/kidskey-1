const FIGERES_COUNT = 10;

import Circle from './classes/Circle.js';
import Triangle from './classes/Triangle.js';
import Square from './classes/Square.js';

let app
app = new PIXI.Application({
	width: 600,
	height: 600,
	backgroundColor: 0xAAAAAA,
});
document.body.appendChild(app.view);

const circle = new Circle(app.screen.width / 7, app.screen.height - 50);
circle.printMain();
app.stage.addChild(circle)

const square = new Square(3 * app.screen.width / 7, app.screen.height - 75);
square.printMain();
app.stage.addChild(square)

const triangle = new Triangle(5 * app.screen.width / 7, app.screen.height - 75);
triangle.printMain();
app.stage.addChild(triangle)

let figure, num;

for (let i = 0; i < FIGERES_COUNT; i++) {
	num = randomInt(0, 2);
	switch (num) {
		case 0:
			figure = new Circle(randomInt(40, app.view.width - 140), randomInt(40, app.view.height - 140))
			break;
		case 1:
			figure = new Square(randomInt(40, app.view.width - 140), randomInt(40, app.view.height - 140));
			break;
		case 2:
			figure = new Triangle(randomInt(40, app.view.width - 140), randomInt(40, app.view.height - 140))
			break;
	}

	figure.interactive = true;
	figure.buttonMode = true;
	figure.print();

	figure
		// events for drag start
		.on('mousedown', onDragStart)
		.on('touchstart', onDragStart)
		// events for drag end
		.on('mouseup', onDragEnd)
		.on('mouseupoutside', onDragEnd)
		.on('touchend', onDragEnd)
		.on('touchendoutside', onDragEnd)
		// events for drag move
		.on('mousemove', onDragMove)
		.on('touchmove', onDragMove);
	app.stage.addChild(figure)
}

function randomInt(min, max) {
	let rand = min + Math.random() * (max + 1 - min);
	return Math.floor(rand);
}

function onDragStart(event) {
	this.data = event.data;
	this.alpha = 0.5;
	this.dragging = true;
}

function onDragEnd() {
	if (boxesIntersect(this, circle)) {
		if (this instanceof Circle) {
			app.stage.removeChild(this);
		} else {
			this.x = this.posX;
			this.y = this.posY;
		}
	}
	if (boxesIntersect(this, square)) {
		if (this instanceof Square) {
			app.stage.removeChild(this);
		} else {
			this.x = this.posX;
			this.y = this.posY;
		}
	}
	if (boxesIntersect(this, triangle)) {
		if (this instanceof Triangle) {
			app.stage.removeChild(this);
		} else {
			this.x = this.posX;
			this.y = this.posY;
		}
	}

	this.posX = this.x;
	this.posY = this.y;
	this.alpha = 1;
	this.dragging = false;
	// set the interaction data to null
	this.data = null;
}

function onDragMove() {
	if (this.dragging) {
		var newPosition = this.data.getLocalPosition(this.parent);
		this.position.x = newPosition.x;
		this.position.y = newPosition.y;
	}
}

function boxesIntersect(a, b) {
	var aBox = a.getBounds();
	var bBox = b.getBounds();
	return aBox.x + aBox.width > bBox.x && aBox.x < bBox.x + bBox.width && aBox.y + aBox.height > bBox.y && aBox.y < bBox.y + bBox.height;
}